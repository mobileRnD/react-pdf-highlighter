import React, { Component } from "react";
import "!style-loader!css-loader!../style/Highlight.css";
import { getIcon } from './Icon';

class Highlight extends Component {
  render() {
    const {
      position,
      onClick,
      onMouseOver,
      onMouseOut,
      comment,
      isScrolledTo,
      color
    } = this.props;
    const {
      rects,
      boundingRect
    } = position;
    const icon = getIcon(comment);
    return React.createElement("div", {
      className: `Highlight ${isScrolledTo ? "Highlight--scrolledTo" : ""}`
    }, comment ? React.createElement("div", {
      className: "Highlight__emoji",
      style: {
        left: 20,
        top: boundingRect.top
      }
    }, icon) : null, React.createElement("div", {
      className: "Highlight__parts"
    }, rects.map((rect, index) => React.createElement("div", {
      onMouseOver: onMouseOver,
      onMouseOut: onMouseOut,
      onClick: onClick,
      key: index,
      style: { ...rect,
        background: isScrolledTo ? null : color || '#D3DAFE'
      },
      className: `Highlight__part`
    }))));
  }

}

export default Highlight;