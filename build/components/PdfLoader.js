import React, { useState, useEffect } from "react";
import pdfjs from "pdfjs-dist";
import pdfjsWorker from "pdfjs-dist/build/pdf.worker.entry";

const PdfLoader = props => {
  const [pdfDocument, setPdfDocument] = useState(null);
  const {
    url,
    onError
  } = props;
  useEffect(() => {
    pdfjs.getDocument({
      url: url,
      eventBusDispatchToDOM: true
    }).promise.then(pdfDocument => {
      setPdfDocument(pdfDocument);
    }).catch(onError);
    return setPdfDocument(null);
  }, [url]);
  const {
    children,
    beforeLoad
  } = props;

  if (pdfDocument) {
    return children(pdfDocument);
  }

  return beforeLoad;
};

export default PdfLoader;